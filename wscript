#! /usr/bin/env python
# encoding: utf-8

APPNAME = 'CSACwithVoid'
VERSION = '1.0'

srcdir = '.'
builddir = 'build'

bioD_dir = '/home/yuichi/Workspace/BioD'
bioD_src_dir = '/home/yuichi/Workspace/BioD/bio'
bioD_lib_dir = '/home/yuichi/Workspace/BioD/build'
bioD_libname = 'bio'


def configure(conf):
    # Choose d compiler
    d_compilers = ['ldmd2', 'dmd']
    #d_compilers = ['dmd', 'ldmd2']
    
    for d_compiler in d_compilers:
        try:
            conf.find_program(d_compiler, var='D_COMPILER')
        except:
            continue
        else:
            break
    else:
        conf.fatal('D compiler ' + str(d_compilers) + ' are not found.')

    # Release option
    conf.env.append_value('DFLAGS', ['-O', '-release', '-boundscheck=off', '-inline',
                                     '-I' + bioD_dir,
                                     '-L-L' + bioD_lib_dir,
                                     '-L-l' + bioD_libname])

    # Unittest option
    conf.env.append_value('DFLAGS_TEST', ['-unittest', '-main',
                                          '-I' + bioD_dir,
                                          '-L-L' + bioD_lib_dir,
                                          '-L-l' + bioD_libname])

    
def build(bld):
    # compile
    bld(rule = '${D_COMPILER} ${DFLAGS} ${SRC} -of${TGT}',
        source = 'CSACwithVoid.d',
        target = APPNAME)
    
    bld(rule = '${D_COMPILER} ${DFLAGS} ${SRC} -of${TGT}',
        source = 'latticePointComparison.d',
        target = 'latticePointComparison')

    bld(rule = '${D_COMPILER} ${DFLAGS} ${SRC} -of${TGT}',
        source = 'forPlotLatticePoint.d',
        target = 'forPlotLatticePoint')

    bld(rule = 'cp ${SRC} .',
        source = 'plotSphericalLatticePoints.gp')

    bld(rule = 'cp ${SRC} .',
        source = 'plotSphericalCapL2Discrepancies.gp')
    
    # unit test
    #bld(rule = '${D_COMPILER} ${DFLAGS_TEST} -run ${SRC}',
    #    source = 'CSACwithVoid.d')
