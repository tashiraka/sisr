// Writen in the D programming language.

/**
 * C-SAC with the void constraint.
 *
 * Authors: Yuichi Motai 
 */
import std.stdio, std.algorithm, std.array, std.random, std.math, std.conv, std.json,
  std.file, std.parallelism, std.range;
import bio.geometry;


version (unittest) {}
 else {
   void main (string[] args)
   {
     auto jsonFile = args[1]; // Assume UTF-8 file
     auto parameters = new Parameters(jsonFile);
     auto systems = CSACwithVoid(parameters);
     auto fout = File(parameters.outFile, "w");
     fout.writeln(parameters.toString);
     foreach (i, system; systems)
       fout.writeln("@system ", system.weight, "\n", system.toString(i));
   }
 }


System[] CSACwithVoid(Parameters parameters)
{
  // Fibonacci lattice in unit sphere and pre remove the adjascent bead intersection
  auto f = getSphericalFibonacciLatticePoint(parameters.n);
  
  // Declare the L systems
  auto systems = new System[](parameters.L);

  // Initialize the L systems
  writeln("initializing");
  foreach (ref system; systems)
    system = new System(parameters.centerV.dup, parameters.Rv,
                        parameters.centerC.dup, parameters.Rc,
                        parameters.M, parameters.N, f.map!(x => x.dup).array,
                        parameters.r, parameters.l);
  
  // Elongate the chains in all systems
  auto resamplingCount = 0;
  
  foreach (k; 2..parameters.N) {
    writeln("Elongating ", k + 1, " / ", parameters.N);
    foreach (system; systems) system.elongate;
    //foreach (system; systems.parallel) system.elongate;
    //foreach (system; systems.parallel(systems.length / totalCPUs)) system.elongate;

    standardizeWeights(systems);

    //foreach (system; systems.sort!((a,b) => a.weight < b.weight))
    //writeln(system.weight);
    /*
    // Static resampling in CSAC model
    if ((k + 1) % parameters.resamplingStep == 0) {
      writeln("Static resampling");

      staticResampling(systems, parameters.resamplingBinNum);
    }
    */
    auto ess = systems.ESS;
    writeln("ESS ", ess);
    /*
    // Dynamic resampling in CSAC model
    if (ess < parameters.ESSThresold) {
      writeln("Dynamic resampling at ESS = ", ess);      
      systems = dynamicResampling(systems, k+1);
    }
    */
    if (ess < parameters.ESSThresold) {
      writeln("Dynamic residual resampling");
      systems = residualResampling(systems);
      ++resamplingCount;
    }
  }

  foreach (system; systems) writeln(system.weight);
  writeln("The number of resampling ", resamplingCount);
  
  return systems;
}


void standardizeWeights(ref System[] systems)
{
  double sumWeight = systems.map!(x => x.weight).sum;
  if (sumWeight == 0) throw new Exception("All weights are 0.");
  foreach (ref system; systems) system.changeTopWeight(system.weight / sumWeight);
}


double[] standardize(double[] arr)
{
  auto sum = arr.sum;
  return arr.map!(x => x / sum).array;
}


unittest
{
  double[] arr = [1, 2, 3, 4];
  assert (arr.standardize == [0.1, 0.2, 0.3, 0.4], arr.standardize.to!string);
}


class Parameters
{
  const JSONValue json;
  const ulong L, M, N, n, resamplingStep, resamplingBinNum;
  const Point centerV, centerC;
  const double Rv, Rc, r, l, ESSThresold;
  const string outFile;
  // L % resamplingBinNum = 0

  this(string jsonFile) {
    // Assume UTF-8 file
    auto content = readText(jsonFile);
    json = parseJSON(content);

    L = json.object["theNumberOfSamples"].integer;
    M = json.object["theNumberOfChains"].integer;
    N = json.object["chainLength"].integer;
    n = json.object["indexOfFibonacciNumber"].integer;
    centerV = new Point(json.object["centerOfVoid"].array
                        .map!(x => x.floating).array);
    Rv = json.object["radiusOfVoid"].floating;
    centerC = new Point(json.object["centerOfConfinement"].array
                        .map!(x => x.floating).array);
    Rc = json.object["radiusOfConfinement"].floating;
    r = json.object["radiusOfBead"].floating;
    l = json.object["linkerLength"].floating;
    resamplingStep = json.object["reasamplingInterval"].integer;
    resamplingBinNum = json.object["theNumberOfResamplingBins"].integer; 
    ESSThresold =
      L * json.object["effectiveSampleRatio"].floating;
    outFile = json.object["outFile"].str;

    if (L % resamplingBinNum != 0) {
       throw new Exception("(Sample size) % (The number of resampling bin) != 0");
    }
  }

  this(ulong a_, ulong b_, ulong c_, ulong d_, double[] e_, double f_,
       double[] g_, double h_, double i_, double j_, ulong k_, ulong l_,
       double m_, string n_)
  {
    json = null;
    
    L = a_;
    M = b_;
    N = c_;
    n = d_;
    centerV = new Point(e_);
    Rv = f_;
    centerC = new Point(g_);
    Rc = h_;
    r = i_;
    l = j_;
    resamplingStep = k_;
    resamplingBinNum = l_;
    ESSThresold = L * m_;
    outFile = n_;

    if (L % resamplingBinNum != 0) {
       throw new Exception("(Sample size) % (The number of resampling bin) != 0");
    }
  }
  
  override string toString() { return json.toString; }
}


double ESS(System[] systems)
{
  auto weights = systems.map!(x => x.weight);
  return weights.sum ^^ 2 / weights.map!(x => x ^^ 2).sum;
}


System[] residualResampling(System[] systems)
{
  System[] resampledSystems;
  uint[] k;
  auto mr = systems.length;
  
  standardizeWeights(systems);
  
  foreach (system; systems) {
    k ~= (system.weight * systems.length).to!uint;
    foreach (i; 0..k[$-1]) resampledSystems ~= system.dup;
    mr -= k[$-1];
  }

  if (k.sum > systems.length)
    throw new Exception("k.sum > systems.length :" ~ k.sum.to!string);
  
  if (mr != 0) {
    double[] residuals;
    foreach (i; 0..systems.length) {
      residuals ~= systems[i].weight * systems.length - k[i];
    }

    foreach (i; 0..mr) {
      auto resampledID = dice(residuals);
      resampledSystems ~= systems[resampledID].dup;
    }
  }

  foreach (ref rs; resampledSystems) {
    rs.weightsToOne; 
    rs.incrementStepBackStopper; // prevent step back because of reset of weights
    rs.incrementStepBackStopper; // that is, re-initialization step
  }
  
  return resampledSystems;
}


System[] dynamicResampling(System[] systems, ulong currentStep)
{
  if (systems.length < 2) return systems;

  auto maxW = systems.map!(x => x.weight).reduce!max;
  auto resamplingDistribution =
    systems.map!(x => exp(x.weight - maxW)).array.standardize;
  
  // Inverse transform sampling from resamplingDistribution
  auto newSystems = new System[](systems.length);

  foreach (ref ns; newSystems) {
    auto id = dice(resamplingDistribution);
    ns = systems[id];
    ns.divideWeights(resamplingDistribution[id]);
  }

  /* Too slow
  // Inverse transform sampling from resamplingDistribution
  // The first element of the cumlative distribution is 0.
  auto cumulativeResamplingDistribution = new double[](systems.length + 1);
  cumulativeResamplingDistribution.fill(0.0);
  foreach (i, p; resamplingDistribution) cumulativeResamplingDistribution[i+1] += p;
  
  //Mt19937 gen;
  //gen.seed(unpredictableSeed);

  auto newSystems = new System[](systems.length);

  foreach (i, ref system; systems) {
    auto u = uniform!("[)")(0.0L, cumulativeResamplingDistribution[$-1]);

    // Binary search
    auto id = systems.length / 2; // id in {1, 2, .., systems.length}
    
    while (true) {
      assert(0 < id && id < cumulativeResamplingDistribution.length);
      
      if (cumulativeResamplingDistribution[id - 1] < u &&
          u < cumulativeResamplingDistribution[id]) {
        break;
      }
      else if (u < cumulativeResamplingDistribution[id - 1]) {
        id = id / 2;
      }
      else if (cumulativeResamplingDistribution[id] < u) {
        id = id + (systems.length - id + 1) / 2;
      }
    }

    newSystems[i] = systems[id - 1];
    newSystems[i].divideWeights(resamplingDistribution[id - 1]);
  }
  */
  return newSystems;
}


void staticResampling(System[] systems, ulong resamplingBinNum)
{
  if (systems.length % resamplingBinNum != 0) {
    throw new Exception("(Sample size) % (The number of resampling bin) != 0");
  }

  systems.sort!((a, b) => a.weight < b.weight);
  
  ulong binSize = systems.length / resamplingBinNum;
  // copy top bin to the bottom bin
  foreach (j; 0..binSize) systems[j] = systems[$ - binSize + j].dup;
}
  

/**
unittest
{
  auto f = getSphericalFibonacciLatticePoint(10);
  auto b = new ClosedBall(new Point(1, 1, 1), 10);
  auto systems = new System[](6);

  foreach (i, ref system; systems) {
    system = new System(new Point(0, 0, 0), 10000, new Point(0, 0, 0), 100,
                        3, 10, f, 10, 14);
    system.changeWeight(i);
    assert (system.weight == i);
  }

  systems.staticResampling(3);
  assert(systems[0].weight == systems[4].weight);
  assert(systems[1].weight == systems[5].weight);
}
**/

class System
{
private:
  Chain[] chains;
  Weight w;
  //Mt19937 gen;
  Point[] f;
  uint stepBackStopper = 0;
  
public:
  const Void v;
  const Confinement c;
  const ulong chainLength;
  const ulong chainNum;
  const double beadRadius;
  const double bondLength;
  
  this(inout(Point) centerV, inout(double) Rv, inout(Point) centerC, inout(double) Rc,
       inout(ulong) M, inout(ulong) N, Point[] f_, inout(double) r, inout(double) l)
  {
    //gen.seed(unpredictableSeed);

    v = new Void(centerV, Rv);
    c = new Confinement(centerC, Rc);

    chains = new Chain[](M);
    foreach (ref chain; chains) {
      chain = new Chain(N);
      chain.init;
    }

    w = new Weight(N);
    w.init;
    
    chainLength = N;
    chainNum = M;
    beadRadius = r;
    bondLength = l;
    f = f_;
    
    initializeChain();
  }

  double weight() @property @safe pure { return w.top; }
  void divideWeights(double a) @property @safe pure { w.divideAll(a); }
  void changeTopWeight(double a) @property
  {
    w.pop;
    w.push(a);
  }

  void weightsToOne() { w.allWeightsToOne; }

  void incrementStepBackStopper() { ++stepBackStopper; }

  ulong currentLength()
  {
    auto lens = chains.map!(x => x.currentLength).array;
    auto len = lens[0];
    foreach (l; lens) if (l != len) throw new Exception("Different chain length");
    return len;
  }
  
  string toString(T)(T num)
  {
    auto s = "";
    foreach (i, chain; chains) s ~= chain.toString(num, i) ~ "\n";
    
    return s.strip('\n');
  }

  System dup() @property
  {
    auto dupChains = new Chain[](chains.length);
    foreach (j; 0..chains.length) dupChains[j] = chains[j].dup;
    return new System(dupChains, w.dup, /*gen,*/ v, c, f.map!(x => x.dup).array,
                      chainLength, chainNum, beadRadius, bondLength);
  }
  
private:
  this(Chain[] chains_, Weight w_, /*Mt19937 gen_,*/
       const Void v_, const Confinement c_, Point[] f_,
       const ulong chainLength_, const ulong chainNum_,
       const double beadRadius_, const double bondLength_)
  {
    chains = chains_;
    w = w_;
    //gen = gen_;
    v = v_;
    c = c_;
    f = f_;
    chainLength = chainLength_;
    chainNum = chainNum_;
    beadRadius = beadRadius_;
    bondLength = bondLength_;
  }
  
  void initializeChain(bool stepBackFlag = false)
  { 
    // Set M first beads
    setFirstBeads();

    // Set M second beads
    auto workingW = w.top;
    auto j = 0;
    auto stepBackCount = 0;
    
    while (j < chainNum) {
      auto emptyPoints = filterEmptyPoints(chains[j].firstBead);
      
      if (emptyPoints.length == 0) { // Dead end, one step back
        if (stepBackStopper > 0) { // Prevent step back two times after resampling
          --stepBackStopper;
          w.push(0.0);
          return;
        }
        
        if(stepBackFlag || stepBackCount > 0) { // Step back is only once
          w.push(0.0);
          throw new Exception("Too fast dead end.");
        }
        
        foreach (ref chain; chains) chain.init;
        w.init;
        stepBackFlag = true;
        ++stepBackCount;
        setFirstBeads();
        j = 0;
      }
      else {
        //auto nextCenter = emptyPoints.randomSample(1, gen).array[0];
        auto nextCenter = emptyPoints.randomSample(1).array[0];
        chains[j].push(new Bead(nextCenter, beadRadius));
        workingW *= emptyPoints.length.to!double;
        ++j;
      }
    }

    w.push(workingW);
  }


  void setFirstBeads()
  {
    foreach (j; 0..chainNum) {
      Bead b;
      auto collisionFlag = false;
      auto counter = 0;

      do {
        collisionFlag = false;
        //auto p = randomPointOnSphere(v.radius, gen);
        auto p = randomPointOnSphere(v.radius);
        b = new Bead(p, beadRadius);

        ++counter;
        //if(counter == 1000) { // This condition of stop is bad.
        if(false) { // This condition of stop is bad.
          throw new Exception("Initialization of a system is not effective.");
        }
        
        foreach (jj; 0..j) {
          if (decideCollisionWithBall(b, chains[jj].firstBead)) {
            collisionFlag = true;
            break;
          }
        }
      } while (collisionFlag);

      chains[j].push(b);
    }

    w.init;
    w.push(1.0);
  }

  
public:  
  void elongate(bool stepBackFlag = false)
  {
    // w == 0 means that some chain has reached dead end.
    if (w.top == 0) {
      w.push(0.0);
      return;
    }

    auto workingW = w.top;
    auto j = 0;
    auto stepBackCount = 0;
    
    while (j < chainNum) {
      auto emptyPoints = filterEmptyPoints(chains[j].nextTop);
      
      if (emptyPoints.length == 0) { // Dead end, step back
        if (stepBackStopper > 0) { // Prevent step back two times after resampling
          --stepBackStopper;
          w.push(0.0);
          return;
        }
        
        if(stepBackFlag || stepBackCount > 0) { // Step back is only once
          w.push(0.0);
          return;
        }
        
        if (chains[0].currentLength == 2) { // Need initial step for step back
          foreach (ref chain; chains) chain.init;
          w.init;
          stepBackFlag = true;
          ++stepBackCount;
          initializeChain(stepBackFlag);
          j = 0;        
        }
        else {
          foreach (currentlyPushedID; 0..j) chains[currentlyPushedID].pop;
          foreach (pushedOneStepAgoID; 0..chainNum) chains[pushedOneStepAgoID].pop;
          w.pop;
          foreach (pushedTwoStepAgoID; 0..chainNum) chains[pushedTwoStepAgoID].pop;
          w.pop;
          stepBackFlag = true;
          ++stepBackCount;
          elongate(stepBackFlag); // elongate two step ago
          elongate(stepBackFlag); // elongate one step ago
          j = 0;
        }        
      }
      else {
        //auto nextCenter = emptyPoints.randomSample(1, gen).array[0];
        auto nextCenter = emptyPoints.randomSample(1).array[0];
        chains[j].push(new Bead(nextCenter, beadRadius));
        workingW *= emptyPoints.length.to!double;
        ++j;
      }
    }

    w.push(workingW);
  }
  

private:
  Point[] filterEmptyPoints(Bead b)
  {
    // Transform Fibonacci lattice points around the top bead position.
    return f.map!(x => x * (2.0 * beadRadius + bondLength) + b.center)
      .filter!(x =>
               l2Distance(x, b.center) > 2.0 * b.radius &&
               l2Distance(x, v.center) > v.radius + b.radius &&
               l2Distance(x, c.center) < c.radius - b.radius &&
               !decideCollisionWithAllChains(new Bead(x, beadRadius))).array;
  }
  
  bool decideCollisionWithAllChains(ClosedBall b)
  {
    foreach (chain; chains)
      foreach (j; 0..chain.currentLength)
        if (decideCollisionWithBall(b, chain.beads[j])) return true;
    return false;
  }
}


class Void
{
  const Point center;
  const double radius;

  this (inout(Point) c, inout(double) r) {
    center = c;
    radius = r;
  }
}


class Confinement
{
  const Point center;
  const double radius;
  
  this (inout(Point) c, inout(double) r) {
    center = c;
    radius = r;
  }
}


// Stack-like data structure
class Chain
{
private:
  Bead[] beads;
  long topID = -1;

public:
  this(inout(ulong) M) { beads = new Bead[](M); }

  Bead firstBead() @property @safe pure { return beads[0]; }
  
  Bead top() @property @safe pure { return beads[topID]; }

  Bead nextTop() @property @safe pure
  {
    assert (topID>0);
    return beads[topID - 1];
  }
  
  ulong currentLength() @property @safe pure const { return topID + 1; }

  void push(Bead b) @safe pure {
    ++topID;
    beads[topID] = b;
  }

  Bead pop() @property {
    assert (topID >= 0);
    --topID;
    return beads[topID + 1];
  }

  void init() @property {
    topID = -1;
  }

  /*
  override string toString()
  {    
    assert (topID >= 0);
    
    auto s = "";

    foreach_reverse (i; 0..(topID + 1))
      if (i % 2 == 0) s ~= beads[i].center.toString ~ "\n";
    
    foreach (i; 0..topID)
      if (i % 2 == 1) s ~= beads[i].center.toString ~ "\n";

    return s.strip('\n');
  }
  */
  
  string toString(T)(T num1, T num2)
  {
    assert (topID >= 0);

    auto s = "";
    auto prefix = ">" ~ num1.to!string ~ "_" ~ num2.to!string ~ "\t";

    foreach_reverse (i; 0..(topID + 1))
      if (i % 2 == 0) s ~= prefix ~ beads[i].center.toString ~ "\n";
    
    foreach (i; 0..(topID + 1))
      if (i % 2 == 1) s ~= prefix ~ beads[i].center.toString ~ "\n";

    return s.strip('\n');
  }

  
  Chain dup() @property
  {
    auto dupBeads = new Bead[](beads.length);
    foreach (k; 0..(topID+1)) dupBeads[k] = beads[k].dup;
    return new Chain(dupBeads, topID);
  }

private:
  this(Bead[] beads_, long topID_)
  {
    beads = beads_;
    topID = topID_;
  }
}


unittest
{
  auto chain = new Chain(3);
  assert(chain.currentLength == 0);
  auto b = new Bead(new Point(1, 1, 1), 10);
  auto c = new Bead(new Point(1, 1, 1), 10);
  chain.push(b);
  assert(chain.firstBead == b);
  assert(chain.firstBead != c);
  assert(chain.currentLength == 1);
  assert(chain.pop == b);
  chain.push(b);
  chain.push(c);
  assert(chain.currentLength == 2);
  chain.init;
  assert(chain.currentLength == 0);
}


alias Bead = ClosedBall;


// Working together with Chain class
class Weight
{
private:
  double[] w;
  long topID = -1;

public:
  this(inout(ulong) M) { w = new double[](M); }

  double firstweight() @property @safe pure { return w[0]; }
  
  double top() @property @safe pure { return w[topID]; }

  void divideAll(double a) @property @safe pure { w = w.map!(x => x / a).array; }
  
  ulong currentLength() @property @safe pure const { return topID + 1; }

  void push(double w_) @safe pure {
    ++topID;
    w[topID] = w_;
  }

  double pop() @property {
    assert (topID >= 0);
    --topID;
    return w[topID + 1];
  }

  void init() @property {
    topID = -1;
  }

  void allWeightsToOne() { w.fill(1.0); }
  
  Weight dup() @property
  {
    return new Weight(w.dup, topID);
  }

private:
  this(double[] w_, long topID_)
  {
    w = w_;
    topID = topID_;
  }
}
