zoom = 80

def setup():
    size(650, 600, P3D)
        
        
def draw():
    background(200) #black background
    #translate(width/2, height/2) #set the origin on the center of display
    camera(zoom, 0, 0, 0, 0, 0, 0, 1, 0)
    #rotateX(radians(100))
    #rotateY(radians(100))
    #rotateZ(radians(270))
    rotateY(radians(mouseY))
    rotateZ(radians(mouseX))
    
    path = '/home/yuichi/Workspace/polymer_workbench/randomPolymer/SISR/build'
    lllp_file = path + '/latitude_longitude_lattice_points.xyz'
    lllp_out_file = path + '/latitude_longitude_lattice_points.png'

    scale(1)

    sphere(20)

    for line in open(lllp_file):
        point = [20.0 * float(x) for x in line.rstrip().split('\t')]
        translate(point[0], point[1], point[2])
        sphere(0.1)
        translate(-point[0], -point[1], -point[2])
    
    saveFrame(lllp_out_file)


def mouseWheel(event):
    global zoom 
    zoom += event.getCount() * 20

