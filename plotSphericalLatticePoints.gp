set terminal png

set view equal xyz
set ticslevel 0

set output 'latitude_longitude_lattice_points.png'
splot 'latitude_longitude_lattice_points.xyz' with points pt 7 lc 3

set output 'spherical_Fibonacci_lattice_points.png'
splot 'spherical_Fibonacci_lattice_points.xyz' with points pt 7 lc 3

