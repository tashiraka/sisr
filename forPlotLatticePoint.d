import std.stdio;
import bio.geometry;


version(unittest) {}
 else {
   void main(string[] args)
   {
     // latitude-longitude lattice point (lllp)
     auto lllpOutFile =
       "latitude_longitude_lattice_points.xyz";
     // spherical fibonacci lattice point (sflp)
     auto sflpOutFile =
       "spherical_Fibonacci_lattice_points.xyz";
     auto sizeOutFile =
       "size_of_lattice_points";
     auto sizeFout = File(sizeOutFile, "w");
     
     /**
      * lllp
      */
     auto lllpFout = File(lllpOutFile, "w");
     auto delta = 10; // N is approx 500
     auto lllps = getLatitudeLongitudeLatticePoint(delta);
     foreach(p; lllps) lllpFout.writeln(p);
     sizeFout.writeln("The size of latitude longitude lattice points: ",
                      lllps.length);

     /**
      * sflp
      */
     auto sflpFout = File(sflpOutFile, "w");
     auto n = 15; // N is approx 500
     auto sflps = getSphericalFibonacciLatticePoint(n);
     foreach(p; sflps) sflpFout.writeln(p);
     sizeFout.writeln("The size of spherical Fibonacci lattice points: ",
                      sflps.length);

   }
 }

