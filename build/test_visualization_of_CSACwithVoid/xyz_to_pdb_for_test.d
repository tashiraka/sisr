import std.stdio, std.conv, std.string, std.array, std.algorithm, std.range,
  std.math;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto coordFile = args[1];
     auto outFile = args[2];

     auto coords = readCoord(coordFile);
     auto pdb = coordsToPdb(coords);

     auto fout = File(outFile, "w");
     foreach (entry; pdb) fout.writeln(entry);
   }
 }


double[][] readCoord(string filename)
{
  double[][] coords;
  foreach (line; File(filename, "r").byLine)
    coords ~= line.to!string.strip.split().map!(x => x.to!double).array;
  return coords;
}


string[] coordsToPdb(double[][] coords)
{
  auto maxAbsVal = coords.map!(x => x.map!(x => x.abs).reduce!max).reduce!max;
  auto digitAdjaster = 1.0;
  while (maxAbsVal < 0) {
      maxAbsVal *= 10.0;
      digitAdjaster *= 10.0;
  }

  while (1000 <= maxAbsVal.to!int) {
      maxAbsVal *= 0.1;
      digitAdjaster *= 0.1;
  }

  assert (0 < maxAbsVal && maxAbsVal < 1000);

  writeln(digitAdjaster);
  
  string[] entries;  
  foreach (i, coord; coords) {
      // 1..6 (6)
      auto atom = "HETATM";
      
      // 7..11 (5)
      auto serial = format("%5d", i);
      if(serial.length != 5) throw new Exception("ERROR: serial.length != 5");

      // 13..16 (1 + 4)
      auto name = "     ";

      // 17 (1)
      auto altLoc = " ";

      // 18..20 (3)
      auto resName = "   ";

      // 22 (1 + 1)
      auto chainID = " 0";
      if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");

      // 23..26 (4)
      auto resSeq = "    ";

      // 27 (1)
      auto iCode = " ";

      // 31..38 (3 + 8)
      auto x = format("   %6.3f", coord[0] * digitAdjaster);
      if (x.length < 11) x = repeat(' ', 11 - x.length).to!string ~ x;
      if (x.length != 11) throw new Exception("ERROR: x.length != 11");

      // 39..46 (8)
      auto y = format("%6.3f", coord[1] * digitAdjaster);
      if (y.length < 8) y = repeat(' ', 8 - y.length).to!string ~ y;
      if (y.length != 8) throw new Exception("ERROR: y.length != 8");

      // 47..54 (8)
      auto z = format("%6.3f", coord[2] * digitAdjaster);
      if (z.length < 8) z = repeat(' ', 8 - z.length).to!string ~ z;
      if (z.length != 8) throw new Exception("ERROR: z.length != 8");

      // 55..60 (6)
      auto occupancy = "      ";

      // 61..66 (6)
      auto tempFactor = "      ";

      // 77..78 (10 + 2)
      auto element = "            ";

      // 79..80 (2)
      auto charge = "  ";

      entries ~= atom ~ serial ~ name ~ altLoc ~ resName ~ chainID ~ resSeq
        ~ iCode ~ x ~ y ~ z ~ occupancy ~ tempFactor ~ element ~ charge;
  }

  foreach (i; 0..coords.length-1)
    entries ~= "CONECT"
      ~ format("%5d", i) ~ format("%5d", i+1) ~ "               ";
  
  entries ~= "END   ";
  
  return entries;
}
