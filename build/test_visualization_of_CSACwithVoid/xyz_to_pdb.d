import std.stdio, std.conv, std.string, std.array, std.algorithm;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto coordFile = args[1];
     auto connectivityFile = args[2];
     auto chrIntervalFile = args[3];
     auto outFile = args[4];

     auto coords = readCoord(coordFile);
     auto gapInfo = readConnectivity(connectivityFile);
     auto chrIntervals = readChrInterval(chrIntervalFile);
     auto pdb = coordsToPdb(coords, chrIntervals, gapInfo);

     auto fout = File(outFile, "w");
     foreach(entry; pdb)
       fout.writeln(entry);
   }
 }


double[][] readCoord(string filename)
{
  double[][] coords;
  foreach(line; File(filename, "r").byLine) {
    coords ~= line.to!string.strip.split("\t").map!(x => x.to!double).array;
  }
  return coords;
}


bool[] readConnectivity(string filename)
{
  bool[] gapInfo;
  foreach(line; File(filename, "r").byLine)
    gapInfo
      ~= line.to!string.strip.split("\t")[1] == "connected" ? true : false;
  return gapInfo;
}


uint[][] readChrInterval(string filename)
{
  uint[][] chrIntervals;
  foreach(line; File(filename, "r").byLine) 
    chrIntervals
      ~= line.to!string.strip.split("\t")[1..$].map!(x => x.to!uint).array;
  return chrIntervals;
}


string[] coordsToPdb(double[][] coords, uint[][] chrIntervals, bool[] gapInfo)
{
  string[] entries;
  string[] connectEntries;
  auto chrIDs = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                 "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                 "K", "L", "M", "N", "O", "p", "Q", "R", "S", "T",
                 "U", "v", "W", "X", "Y", "Z",];
  auto chrID = 0;
  auto chrTerminal = chrIntervals.map!(x => x[$-1]);
  auto coordID = 0;
  Tuple!(ulong, bool)[] connection;
  
  foreach(i, isNotMissing; gapInfo) {
    if(isNotMissing) {
      // 1..6 (6)
      auto atom = "HETATM";
      // 7..11 (5)
      auto serial = format("%5d", i);
      if(serial.length != 5) throw new Exception("ERROR: serial.length != 5");
      // 13..16 (1 + 4)
      auto name = "     ";
      // 17 (1)
      auto altLoc = " ";
      // 18..20 (3)
      auto resName = "   ";
      // 22 (1 + 1)
      auto chainID = " " ~ chrIDs[chrID];
      if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");
      // 23..26 (4)
      auto resSeq = "    ";
      // 27 (1)
      auto iCode = " ";
      // 31..38 (3 + 8)
      auto x = format("   %8.3f", coords[coordID][0]);
      if(x.length != 11) throw new Exception("ERROR: x.length != 11");
      // 39..46 (8)
      auto y = format("%8.3f", coords[coordID][1]);
      if(y.length != 8) throw new Exception("ERROR: y.length != 8");
      // 47..54 (8)
      auto z = format("%8.3f", coords[coordID][2]);
      if(z.length != 8) throw new Exception("ERROR: z.length != 8");
      // 55..60 (6)
      auto occupancy = "      ";
      // 61..66 (6)
      auto tempFactor = "      ";
      // 77..78 (10 + 2)
      auto element = "            ";
      // 79..80 (2)
      auto charge = "  ";

      entries ~= atom ~ serial ~ name ~ altLoc ~ resName ~ chainID ~ resSeq
        ~ iCode ~ x ~ y ~ z ~ occupancy ~ tempFactor ~ element ~ charge;

      coordID++;
      connection ~= tuple(i, true);
    }
    else {
      auto serial = format("%5d", i);
      if(serial.length != 5) throw new Exception("ERROR: serial.length != 5");
      entries
        ~= "REMARK 465                                                                    \nREMARK 465 MISSING BIN " ~ serial ~ "                                                  ";

      connection ~= tuple(i, false);
    }

    if(i == chrTerminal[chrID]) {      
      auto ter = "TER   ";
      auto serial = "     ";
      auto resName = "   ";
      auto chainID = " " ~ chrIDs[chrID];
      if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");
                 
      auto resSeq = "    ";
      auto iCode = " ";
      
      entries ~= ter ~ serial ~ resName ~ chainID ~ resSeq ~ iCode;
      chrID++;
      connection ~= tuple(i, false);
    }
  }

  foreach(i, noUse; connection) {
    if(i < connection.length - 1
       && connection[i][1] && connection[i+1][1])
      {
        if(connection[i][0].to!string.length > 5
           || connection[i+1][0].to!string.length > 5)
          throw new Exception("Atom's ID overflows in CONNECT.");
        
        auto id1 = format("%5d", connection[i][0]);
        auto id2 = format("%5d", connection[i+1][0]);
        if(id1.length != 5 || id2.length != 5)
          throw new Exception("ERROR: CONECT length != 5");
        
            entries ~= "CONECT" ~ id1 ~ id2 ~ "               ";
      }
  }

  
  entries ~= "END   ";
  
  return entries;
}
