BINDIR = '/usr/local/bin'
DFLAGS = ['-O', '-release', '-boundscheck=off', '-inline', '-I/home/yuichi/Workspace/BioD', '-L-L/home/yuichi/Workspace/BioD/build', '-L-lbio']
DFLAGS_TEST = ['-unittest', '-main']
D_COMPILER = ['/usr/bin/dmd']
LIBDIR = '/usr/local/lib'
PREFIX = '/usr/local'
