set xlabel '# points'
set ylabel 'Spherical cap L2-discrepancy'
set key at 79000,0.03

plot 'spherical_cap_L2_discripancies_of_latitude_longitude_lattice_points' with points pt 9 lc 3
replot 'spherical_cap_L2_discripancies_of_spherical_Fibonacci_lattice_points' with points pt 7 lc 1

set terminal png
set output 'spherical_cap_L2_discripancies_of_lattice_points_including_key.png'
replot

unset key
set output 'spherical_cap_L2_discripancies_of_lattice_points.png'
replot

