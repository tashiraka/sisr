import std.stdio, std.parallelism;
import bio.geometry;


version(unittest) {}
 else {
   void main(string[] args)
   {
     // latitude-longitude lattice point (lllp)
     auto lllpOutFile =
       "spherical_cap_L2_discripancies_of_latitude_longitude_lattice_points";
     // spherical fibonacci lattice point (sflp)
     auto sflpOutFile =
       "spherical_cap_L2_discripancies_of_spherical_Fibonacci_lattice_points";
     

     /**
      * Compute spherical cap L2 discrepancy for lllp of various sizes
      */
     auto lllpFout = File(lllpOutFile, "w");
     // N is approx 50 ~ 1000
     auto deltas = [45, 30, 20, 18, 15, 12, 10, 6, 5, 3, 2, 1]; 

     foreach(delta; deltas.parallel) {
       auto points = getLatitudeLongitudeLatticePoint(delta);
       lllpFout.writeln(points.length, "\t", points.sphericalCapL2Discrepancy);
     }
     
     /**
      * Compute spherical cap L2 discrepancy for sflp of various sizes
      */
     auto sflpFout = File(sflpOutFile, "w");
     // N is approx 50 ~ 1000
     auto ns = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]; 
     
     foreach(n; ns.parallel) {
       auto points = getSphericalFibonacciLatticePoint(n);
       sflpFout.writeln(points.length, "\t", points.sphericalCapL2Discrepancy);
     }
   }
 }

